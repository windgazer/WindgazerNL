---
title: Star of David
description: A simple logic exercise to get better at programming.
tags: [js]
date: 2018-02-25
---

# Star of David

The Star of David, also known as Solomon's Seal, is a simple shape created by
mirroring and overlapping two perfect triangles. As such this shape can quite
easily be calculated and printed using any for of output.

For now, let's focus on text output. The exercise is to simply practice basic
programming logic. So, here's the shape we're aiming to achieve:

```
      #
     # #
#############
 # #     # #
  #       #
 # #     # #
#############
     # #
      #
```

As input, the rendering above would receive the number 13, which is the longest
axis in this output (the two lines of `#`-characters). Your software should
allow any number as input, but throw an error if that input cannot render a
star. The number 12 would not work, as would be the case for many other numbers.

Provide unit-tests to cover as many cases as you feel appropriate. Make sure you
cover the following input:

- `1` => 'Invalid input, that number cannot result in a star'
- `One` => 'Invalid input, not a number'
- `13` => (the above rendering)

Cheers, and happy programming!

---
title: Lessons
pinned: project
layout: topic
topic: lessons
---

## Some simple material to base programming lessons on

Here I present some material I've created to teach people around me about basic
programming. Since not all of the stuff used in the past was created by me, nor
found by me, this is not an exhaustive list, merely a place for me to keep track
of what I do have.

Just like most of the content in this site, it's licensed with the [smile][l1]
license, meaning you can use it so long as in return you make somebody else
smile, or help them in some constructive manner.

---
title: Casual Connect SF '13
description: A presentation of Real Life audio usage in HTML5 applications.
date: 2013-08-26
---

*This video was originaly published on [https://www.youtube.com/watch?v=VHtPTXUt4X4](https://www.youtube.com/watch?v=VHtPTXUt4X4).*

I did a presentation on 'Audio in HTML5' in San Francisco together with Craig Robinson of
[Absolute Heroes][1], while, at the time of this presentation I am at work for
[Spilgames][2]. The aim was to simply explain how the somewhat scattered support of audio
could be pulled together to make use of it in HTML5 game development.

<iframe width="560" height="315" src="https://www.youtube.com/embed/VHtPTXUt4X4" frameborder="0" allowfullscreen></iframe>

The presentation slides can be downloaded from [Casual Connects' cloud][3]. If at any time
any of this content become unavailable, please let me know, I have backups that I will
make available at such a time.

<style>iframe { display: block; margin: 1em auto; }</style>

[1]: http://absoluteherogames.com/
[2]: http://spilgames.nl/
[3]: https://s3.amazonaws.com/Casual_Connect_USA_2013_Presentations/HTML5_CCUSA_2013.ppt

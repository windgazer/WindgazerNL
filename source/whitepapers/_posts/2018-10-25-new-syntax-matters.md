---
title: New Syntax might matter more than you think
description: Async/await is not just a fancy way to write a Promise chain. A short article to explain how the new parts of a language might surprise you and help out.
date: 2018-10-25
layout: redirect
metarefresh: https://medium.com/bestseller-e-commerce-technology/new-syntax-might-matter-more-than-you-think-ae36f4e625d0
---

*This article was originaly published in dutch on [https://medium.com/bestseller-e-commerce-technology/new-syntax-might-matter-more-than-you-think-ae36f4e625d0](https://medium.com/bestseller-e-commerce-technology/new-syntax-might-matter-more-than-you-think-ae36f4e625d0).*

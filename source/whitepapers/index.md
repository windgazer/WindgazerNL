---
title: White-papers
pinned: whitepaper
layout: topic
topic: whitepapers
---

## Writings and musings on technical subjects

Whenever I should come around to really publish an article that is written well enough to
follow through, I would like to publish it somewhere. While nothing lasts forever, I will
also keep a copy here, on my own site, where it's continued existence, or early demise, is
entirally in my own hands.

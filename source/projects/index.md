---
title: Projects
pinned: project
layout: topic
topic: projects
---

## My Projects and Tech ramblings

This is a page that should allow you to jump into my technological ramblings. Be they
completely random and for my own education and/or amusement, or rather for actual money. I
should warn you though, it may get crazy ;).

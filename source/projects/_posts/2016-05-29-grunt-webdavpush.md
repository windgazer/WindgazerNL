---
title: Grunt-WebdavPush
date: 2016-05-29
description: A grunt plugin for pushing changed files to a webdav-enabled server, without rsync dependency.
layout: redirect
tags: [js, node, project]
metarefresh: https://www.npmjs.com/package/grunt-webdavpush
---

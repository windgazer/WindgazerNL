---
title: Keybind Companion
date: 2015-04-30
description: An app for showing a layout of your keybinds.
layout: redirect
tags: [js, html5, css, project]
metarefresh: http://pebble.windgazer.nl/KeybindCompanion/#tartarus
---

---
title: Big Buck Bunny, CSS experimentations
permalink: /projects/cssrepository/parallax/landscape.html
---

## Big Buck Bunny, CSS experimentations

This is a demo using [my own CSS3 parallax][1] invention. If you want to understand what's
so special about this demo, [read the arcticle][1] :)


Although I was hoping to come up with a slightly more inspired final result, I found that
retro 8-bit games don't lend themselves very well. The main problem with 8-bit retro games
is that parallax control in those games is only expected to work horizontally (for good
reason). As a result I had to find a quick alternative and looked up some screenshots from
Big Buck Bunny. I used photoshop to smart replace any character/objects out of the grassy
slopes and then cut the slopes into separate parts, padded them out for some overlap and
threw them into my parallax:

<div class="viewport hoverParallax">
	<a id="angle1" href="#angle1">Angle 1</a>
	<a id="angle2" href="#angle2">Angle 2</a>
	<a id="angle3" href="#angle3">Angle 3</a>
	<a id="angle4" href="#angle4">Angle 4</a>
	<a id="angle5" href="#angle5">Angle 5</a>
	<a id="angle6" href="#angle6">Angle 6</a>
	<a id="angle7" href="#angle7">Angle 7</a>
	<a id="angle8" href="#angle8">Angle 8</a>
	<a id="angle9" href="#angle9">Angle 9</a>
	<a id="angle10" href="#angle10">Angle 10</a>
	<a id="angle11" href="#angle11">Angle 11</a>
	<a id="angle12" href="#angle12">Angle 12</a>
	<a id="angle13" href="#angle13">Angle 13</a>
	<a id="angle14" href="#angle14">Angle 14</a>
	<a id="angle15" href="#angle15">Angle 15</a>
	<a id="angle16" href="#angle16">Angle 16</a>
	<div class="canvas">
		<div class="layer layer1">Center</div>
		<div class="layer layer2">Background</div>
		<div class="layer layer3">Background</div>
		<div class="layer layer4">Background</div>
		<div class="layer layer_1">Foreground</div>
	</div>
</div>

Some people were wondering at the title of this page, it stems from this <a href="http://entertainment.desktopnexus.com/wallpaper/332689/" rel="external">wallpaper</a> of the <a href="http://www.bigbuckbunny.org/" rel="external">Big Buck Bunny</a> open source movie.

<style type="text/css">
	.hoverParallax {
		width: 860px;
		height: 300px;
		position: relative;
		background: black;
		color: white;
		overflow: hidden;
		margin: 1em auto;
	}

	.hoverParallax > a {
		position: absolute;
		top: 0;
		left: 0;
		right: 75%;
		bottom: 75%;
		text-indent: -100em;
		overflow: hidden;
		z-index: 1000;
	}

	.hoverParallax > a:nth-of-type(4n+2) {
		left: 25%;
		right: 50%;
}
	.hoverParallax > a:nth-of-type(4n+3) {
		left: 50%;
		right: 25%;
}
	.hoverParallax > a:nth-of-type(4n+4) {
		left: 75%;
		right: 0%;
}
	.hoverParallax > a:nth-of-type(1n+5) {
		top: 25%;
		bottom: 50%;
	}
	.hoverParallax > a:nth-of-type(1n+9) {
		top: 50%;
		bottom: 25%;
	}
	.hoverParallax > a:nth-of-type(1n+13) {
		top: 75%;
		bottom: 0;
	}

	.hoverParallax .canvas {
		width: 1000px;
		height: 1000px;
		margin-top: -500px;
		margin-left: -500px;
		position: absolute;
		top: 50%;
		left: 50%;
	-webkit-transition: all 1s ease-out;
	-moz-transition: all 1s ease-out;
	transition: all 1s ease-out;
	}

	.hoverParallax > a:hover ~ .canvas {
    	width: 50px;
    	margin-left: -25px;
    	height: 500px;
    	margin-top: -250px;
	}
	.hoverParallax > a:nth-of-type(4n+2):hover ~ .canvas {
    	width: 750px;
    	margin-left: -375px;
    }
	.hoverParallax > a:nth-of-type(4n+3):hover ~ .canvas {
    	width: 1250px;
    	margin-left: -625px;
    }
	.hoverParallax > a:nth-of-type(4n+4):hover ~ .canvas {
    	width: 1950px;
    	margin-left: -975px;
    }
	.hoverParallax > a:nth-of-type(1n+5):hover ~ .canvas {
    	height: 750px;
    	margin-top: -375px;
	}
	.hoverParallax > a:nth-of-type(1n+9):hover ~ .canvas {
    	height: 1250px;
    	margin-top: -625px;
	}
	.hoverParallax > a:nth-of-type(1n+13):hover ~ .canvas {
    	height: 1950px;
    	margin-top: -975px;
	}

	.hoverParallax .layer {
		width: 1024px;
		height: 460px;
		position: absolute;
		margin-top: -220px;
		margin-left: -502px;
		top: 49%;
		left: 49%;
		background: transparent url(/projects/cssrepository/bigbuck/layer0.png) no-repeat 50% 50%;
		z-index: 100;
	}

	.hoverParallax .layer2 {
		top: 48.5%;
		left: 48%;
		margin-top: -215px;
		margin-left: -492px;
		background: transparent url(/projects/cssrepository/bigbuck/layer1.png) no-repeat 50% 50%;
	z-index: 99;
	}

	.hoverParallax .layer3 {
		top: 48%;
		left: 47%;
		margin-top: -210px;
		margin-left: -482px;
		background: transparent url(/projects/cssrepository/bigbuck/layer2.png) no-repeat 50% 50%;
	z-index: 98;
	}

	.hoverParallax .layer4 {
		top: 47.5%;
		left: 46%;
		margin-top: -205px;
		margin-left: -472px;
		background: transparent url(/projects/cssrepository/bigbuck/layer3.png) no-repeat 50% 50%;
	z-index: 97;
	}

	.hoverParallax .layer_1 {
		top: 55%;
		left: 55%;
		margin-top: -250px;
		margin-left: -562px;
		background: transparent url(/projects/cssrepository/bigbuck/tree_foreground.png) no-repeat 80% 45%;
	z-index: 101;
	}
</style>

[1]: /projects/cssrepository/2012/01/12/parallax-nojs/

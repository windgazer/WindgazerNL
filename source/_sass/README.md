# SASS in Atomic

Working at Bestseller.com, a colleague and I came up with a principle that would encourage
a high degree of Separation of Concerns and make using scss as a styling language easy to
maintain. It's a amalgamation of 'The inverted Triangle', BEM, OOCSS and Atomic CSS.

However, although it borrows from all of them, from the perspective of most of them, this
would seem to be an anti-pattern. Our argument was simple, DRY (Don't Repeat Yourself) is
impossible to achieve, you will have to repeat yourself somewhere... So then the question
should be, where can I repeat myself while still keeping development manageable. Atomic
CSS doesn't break the SoC, HTML is tightly coupled with style by introducing highly
opinionated classes into the HTML structure. BEM suffers less from this perspective, but
you'd still easily repeat property definitions in your CSS code. The inverted triangle is
a description of how to make your CSS manageable, but fails to provide a clear answer on
how to apply those styles.

So, Atomic SCSS...

## Atomic, how?

I encourage you to google some more on 'Atomic CSS' to understand the base principle, we've
chosen the following terms:

- Boson (Definition of variables)
- Hadron (Use variables, define properties)
- Atom (Use properties, define visual elements)
- Molecule (Use visual elements, define reusable patterns)
- Organism (Use patterns, link to the real world)
- Electrons (Define reusable functionality)

Organisms are the only definitions that are linked to actual classnames, which should be
semantic and BEM style. At all other levels we're using functional BEM style names and
SCSS placeholders. As the definition already implies, make sure that each level only
extends one level down.

```scss
$b__color__primary: #000;
$b__color__primary--offset: #fff;
$b__whitespace: 1rem;

%h__color__primary {
    color: $b__color__primary;
}

%h__color__background__primary {
    background-color: $b__color__primary--offset;
}

%h__whitespace__box-padding {
    padding: $b__whitespace (2 * $b__whitespace);
}

%a__content--color {
    @extend %h__color__primary;
    @extend %h__color__background__primary;
}

%a__content__box {
    @extend %h__whitespace__box-padding;
}

%m__content-box--default {
    @extend %a__content--color;
    @extend %a__content__box;
}
```

On the face of it, this methodology seems much more work to apply to real-world code. But
I wanted to test it on a small-scale design because I believe it is actually no less
expensive than more traditional methods, while being far more maintainable.

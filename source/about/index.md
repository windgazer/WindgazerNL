---
title: About
tags:
  - about
  - info
  - personal
---

## About windgazer.nl

This homepage is build to allow for a place where I hope to help you enjoy creating this
illusion we call Internet as much as I do. This website should, hopefully, be a source for
helping out you, or other programmers, in finding a different solution to both common and
uncommon problems.

At the same time it will serve to show-case my skills and hopefully get a bit of a
reflection of what my peers and betters think of what I do.

All solutions are published under '[☺ License][l]' unless indicated otherwise. All
publications remain copywrited by me and a written acknowledgement is required to use
these materials for any purpose other than personal archiving for later use.

## About me

Right, me, uhm, where do I start. Let's start simple, I'm Martin "Windgazer" Reurings.
I'm a male blonde god from the insignificantly small Netherlands. You know, that country
nobody can find, with it's people spread all over the world infiltrating your
trade-systems? No really, small country, big mouth and I'm no god either, I wish ;)

I've lived here all of my live, born, bred, probably will die here and I'm a part-time
backpacker, never quite at home for an extended amount of years, but I can't stay away
from home either.

In between my short bouts of backpacking I work in IT-related jobs, been there done that,
bought the cd-rom. I sold computers, built computers, repaired, demolished, installed,
programmed, cracked, hacked and fried computers. I remember the days when we had radio
broadcasts from which we could tape C64 games and I've bitfucked in assembly, but that's
the good ol' days.

These days I'm a front-side specialist, I help create the visual illusion that everybody
nowadays calls 'the Internet'. To this end you can find me on [Github][1], [Bitbucket][2],
[experts-exchange][3], [stackoverflow][4], [LinkedIn][5] (in various interest groups) or
[Google+][6]. That's hardly an exhaustive list, but it gets you the general picture.

I also have a social life, but I bet you didn't come here to read about that anyways ;).

## Credits

Some silliness you might want to know about:

- Powered by Linux
- Powered by Apache
- Programmed using Atom.io
- Rendered using Jekyll

and build listening to U2, and all that jazz

[l]: /smile.html
[1]: https://github.com/windgazer-freelance/
[2]: https://bitbucket.com/windgazer/
[3]: http://www.experts-exchange.com/members/mreuring.html
[4]: http://stackoverflow.com/users/2546615/
[5]: https://nl.linkedin.com/in/reurings
[6]: https://plus.google.com/+MartinReurings
